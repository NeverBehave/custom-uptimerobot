<?php
return [
	'title' => 'NoticeBoard',
	'base_url' => 'https://status.hfi.me/',
	'owner_name' => 'HFIProgramming',
	'owner_url' => 'http://hfiprogramming.club',
];